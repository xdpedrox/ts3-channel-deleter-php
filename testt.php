<?php 

class CrawlData
{
  public function __construct($ts3_ServerInstance, $ts3_VirtualServer, $mysqlconn){

    require('config.php');
    require('lang.php');
    
    $this->$starttime = microtime(true);
    $this->ts3_VirtualServer = $ts3_VirtualServer;
    $this->$ts3_ServerInstance = $ts3_ServerInstance;
    $this->$mysqlcon = $mysqlconn;

    $this->$lang = $lang;
    $this->$warnIconTime = $warnIconTime;
    $this->$deleteIconTime = $deleteIconTime;
    $this->$queryname = $queryname;
    $this->$queryname2 = $queryname2;
  

    //Get the time
    $this->$todaydate = time();

    // $icontime = $this->$todaydate - $warntime;
    $this->$warnTime = $this->$todaydate - $warnIconTime;
    $this->$deleteTime = $this->$todaydate - $deleteIconTime;
  }
    
  function setIcon($iconTime, $iconId, $userinchannel, $table_channel, $channel, $channelid, $channelpath) {
    if ($userinchannel > 0) {
      echo '<td><span class="green">' . sprintf($this->$lang['cidup'], $userinchannel) . '</span></td></tr>';
      $this->$mysqlcon->query("UPDATE $table_channel SET lastuse='$this->$todaydate',path='$channelpath' WHERE cid='$channelid'");
      if ($seticon == 1) {
        if ($channel['channel_icon_id'] == $iconId) {
          $channel->permRemoveByName('i_icon_id');
        }
      }
    } else {
      $lastusetime = $this->$mysqlcon->query("SELECT lastuse FROM $table_channel WHERE cid='$channelid'");
      $lastusetime = $lastusetime->fetch_row();
      $this->$mysqlcon->query("UPDATE $table_channel SET path='$channelpath' WHERE cid='$channelid'");
      echo '<td><span class="red">' . $this->$lang['cidnoup'] . '</span></td>';

      if ($seticon == 1 && !in_array($channelid, $nodelete) && $lastusetime[0] < $iconTime && !$channel->isSpacer()) {
        $children = $channel->getChildren();
        if ($children == "") {
          echo '<td><span class="blue">' . $this->$lang['seticon'] . '</span></td>';
          $channel->permAssignByName('i_icon_id', $iconId);
        }
      }
        echo '</tr>';
      }
    }


  public function setNickName() {
    try {
        $this->$ts3_VirtualServer->selfUpdate(array('client_nickname' => $this->$queryname));
    } catch (Exception $e) {
      try {
        $this->$ts3_VirtualServer->selfUpdate(array('client_nickname' => $this->$queryname2));
      } catch (Exception $e) {
        echo '<span class="red"><b>' . $this->$lang['error'] . $e->getCode() . ':</b> ' . $e->getMessage() . '</span><br>';
      }
    }
  }

  public function run() {
    // $this->setNickName();

    //Get a list of all channels
    var_dump($this->$ts3_VirtualServer);
    $tschanarr = $this->$ts3_VirtualServer->channelList();

    //Guardar todos os Cid dentro de uma array
    foreach ($tschanarr as $channel) {
      $tscid[] = $channel['cid'];
    }

    //Apagar todos os Icons
    if ($deleteicons == 1) {
      echo '<b>' . $this->$lang['hldelicon'] . '</b><br>';
      $count = 0;
      foreach ($tschanarr as $channel) {
        if ($channel['channel_icon_id'] == $this->deleteIconTime) {
          $channel->permRemoveByName('i_icon_id');
        }
        if ($channel['channel_icon_id'] ==  $this->$warnIconTime) {
            $channel->permRemoveByName('i_icon_id');
          }
      }
      if ($count > 0) {
        echo $count . $this->$lang['icondel'] . '<br><br>';
      } else {
        echo $this->$lang['iconnodel'] . '<br><br>';
      }
    }

    echo '<b>' . $this->$lang['hlcrawl'] . '</b><br>';
    echo '<table>';
    foreach ($tschanarr as $channel) {
      $channelid = $channel['cid'];
      $channelname = $channel['channel_name'];
      $channelname = htmlspecialchars($channelname, ENT_QUOTES);
      $userinchannel = $channel['total_clients'];
      $chauserinchannelnnelpath = $channel->getPathway();
      $channelpath = htmlspecialchars($channelpath, ENT_QUOTES);

      echo '<tr><td>' . $this->$lang['cid'] . $channelid . ' : </td><td>' . $channelname . '</td>';

      $cidexists = $this->$mysqlcon->query("SELECT * FROM $table_channel WHERE cid='$channelid'");
      $cidexists = $cidexists->num_rows;

      if ($cidexists > 0) {
        $this->setIcon($warnTime, $warnIconTime, $userinchannel, $table_channel, $channel, $channelid, $channelpath);
        $this->setIcon($deleteTime, $deleteiconId, $userinchannel, $table_channel, $channel, $channelid, $channelpath);
      } else {
        echo '<td><span class="blue">' . $this->$lang['record'] . '</span></td></tr>';
        $this->$mysqlcon->query("INSERT INTO $table_channel (cid, lastuse, path) VALUES ('$channelid','$this->$todaydate','$channelpath')");
      }
    }
    echo '</table><br><b>' . $this->$lang['hlcleandb'] . '</b><br><table>';
    $count = 1;
    $cidexists = $this->$mysqlcon->query("SELECT * FROM $table_channel");
    while ($row = $cidexists->fetch_row()) {
      if (!in_array($row[0], $tscid)) {
        echo '<tr><td>' . $this->$lang['cid'] . $row[0] . ' : </td><td>' . $row[2] . '</td><td><span class="green">' . $this->$lang['cleandb'] . '</span><br></td></tr>';
        $count = $count + 1;
        if (!$this->$mysqlcon->query("DELETE FROM $table_channel WHERE cid=$row[0]")) {
          printf("Errormessage: %s\n", $this->$mysqlcon->error);
        }
      }
    }
    echo '</table>';
    if ($count == 1) {
      echo '<span class="red">' . $this->$lang['nodel2'] . '</span><br>';
    }
  
    $buildtime = microtime(true) - $starttime;
    echo '<br>' . sprintf($this->$lang['sitegen'], $buildtime);
  }
}

?>