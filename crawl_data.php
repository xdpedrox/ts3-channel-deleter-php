<?php
$starttime=microtime(true);
?>
<!-- <!doctype html>
<html>
<head>
  <title>TS-N.NET Channeldeleter - Crawl Date</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" href="style.css" type="text/css">
</head>   -->
<body>
<?php

require_once('config.php');
require_once('lang.php');
require_once('TeamSpeak3/TeamSpeak3.php');

try {
	$ts3_ServerInstance = TeamSpeak3::factory("serverquery://".$cfg["user"].":".$cfg["pass"]."@".$cfg["host"].":".$cfg["query"]."/");
	$ts3_VirtualServer = TeamSpeak3::factory("serverquery://".$cfg["user"].":".$cfg["pass"]."@".$cfg["host"].":".$cfg["query"]."/?server_port=".$cfg["voice"]);
	
	require_once('mysql_connect.php');
	
	try {
		$ts3_VirtualServer->selfUpdate(array('client_nickname'=>$queryname));
	} catch(Exception $e) {
		try {
			$ts3_VirtualServer->selfUpdate(array('client_nickname'=>$queryname2));
		} catch(Exception $e) {
			echo'<span class="red"><b>'.$lang['error'].$e->getCode().':</b> '.$e->getMessage().'</span><br>';
		}
	}

	$todaydate=time();

	$warnIconTime=$todaydate-$warnTime;
	$deleteIconTime=$todaydate-$deleteTime;

	$tschanarr=$ts3_VirtualServer->channelList();

	foreach($tschanarr as $channel) {
		$tscid[]=$channel['cid'];
	}

	if ($deleteicons == 1) {
		echo'<b>'.$lang['hldelicon'].'</b><br>';
		$count=0;

		foreach($tschanarr as $channel) {
			$channelid = $channel['cid'];
			$checkicon = $ts3_VirtualServer->channelPermList($channelid, $permsid=FALSE);
			foreach($checkicon as $rows) {
        if ($channel['channel_icon_id'] == $warnIconId || $channel['channel_icon_id'] == $deleteIconId) {
          $channel->permRemoveByName('i_icon_id');
        }
			}
		}
		if ($count > 0) {
			echo $count.$lang['icondel'].'<br><br>';
		} else {
			echo $lang['iconnodel'].'<br><br>';
		}
	}

  echo "Todays date: $todaydate - ";

	foreach($tschanarr as $channel) {
		$channelid = $channel['cid'];
		$channelname = $channel['channel_name'];
		$channelname = htmlspecialchars($channelname, ENT_QUOTES);
		$userinchannel = $channel['total_clients'];
				
		$cidexists = $mysqlcon->query("SELECT * FROM $table_channel WHERE cid='$channelid'");
		$cidexists = $cidexists->num_rows;

		if ($cidexists > 0) {
			if ($userinchannel > 0) {
        if (!$mysqlcon->query("UPDATE $table_channel SET lastuse='$todaydate' WHERE cid='$channelid'")) {
          printf("Errormessage: %s\n", $mysqlcon->error);
        }
        if ($seticon == 1) {
          if ($channel['channel_icon_id'] == $warnIconId || $channel['channel_icon_id'] == $deleteIconId) {
            $channel->permRemoveByName('i_icon_id');
          }
        }
			} else {
				$lastusetime = $mysqlcon->query("SELECT lastuse FROM $table_channel WHERE cid='$channelid'");
				$lastusetime = $lastusetime->fetch_row();
        
        $warn = $lastusetime[0]<$warnIconTime;
        $delete = $lastusetime[0]<$deleteIconTime;
        $isInNoDeleteList = !in_array($channelid, $nodelete);
        $isSpacer = substr_count(strtolower($tschanarr[$channelid]['channel_name']), "spacer") == 0;

				if (($warn || $delete) && $seticon == 1 && $isInNoDeleteList && $isSpacer) {
          $children=$channel->getChildren();
					if ($children == "") {
            if ($warn && !$delete) {
              $channel->permAssignByName('i_icon_id', $warnIconId);
            }
            if ($delete) {
              $channel->permAssignByName('i_icon_id', $deleteIconId);
            }
          }
        }
      }
		} else {
      $mysqlcon->query("INSERT INTO $table_channel (cid, lastuse) VALUES ('$channelid','$todaydate')");
    }
	}
	$count = 1;
	$cidexists=$mysqlcon->query("SELECT * FROM $table_channel");

	while($row=$cidexists->fetch_row()) {
		if (!in_array($row[0], $tscid)) {
			$count = $count + 1;
			if (!$mysqlcon->query("DELETE FROM $table_channel WHERE cid=$row[0]")) {
				printf("Errormessage: %s\n", $mysqlcon->error);
			}
		}
	}

	if ($count == 1) {
		echo'<span class="red">'.$lang['nodel2'].'</span><br>';
	}
}

catch(Exception $e) {
	echo'<span class="red"><b>'.$lang['error'].$e->getCode().':</b> '.$e->getMessage().'</span><br>';
}

$buildtime = microtime(true)-$starttime;
echo'<br>'.sprintf($lang['sitegen'],$buildtime);

?>
</body>
</html>
